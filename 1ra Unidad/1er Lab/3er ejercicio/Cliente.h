#include <iostream>
using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente {
    private:
        string nombre;
        string telefono;
        int saldo;
        bool morosidad;

    public:
        /* constructores */
        Cliente ();
        Cliente (string nombre, string telefono, int saldo, bool morosidad);
        
        /* métodos get */
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_morosidad();

        /* métodos set */
        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_morosidad(bool morosidad);
        
};
#endif