#include <iostream>
#include "Cliente.h"
using namespace std;

///////////////////////////////////////////////////////////////////////
/* constructores */
Cliente::Cliente() {
    string nombre;
    string telefono;
    int saldo;
    bool morosidad;
}
Cliente::Cliente (string nombre, string telefono, int saldo, bool morosidad) {
    this->nombre = nombre;
    this->telefono = telefono;
    this->saldo = saldo;
    this->morosidad = morosidad;
}

/* métodos get */
string Cliente::get_nombre(){
    return this->nombre;
}
string Cliente::get_telefono(){
    return this->telefono;
}
int Cliente::get_saldo(){
    return this->saldo;
}
bool Cliente::get_morosidad(){
    return this->morosidad;
}

/* métodos set */
void Cliente::set_nombre(string nombre){
    this->nombre = nombre;
}
void Cliente::set_telefono(string telefono){
    this->telefono = telefono;
}
void Cliente::set_saldo(int saldo){
    this->saldo = saldo;
}
void Cliente::set_morosidad(bool morosidad){
    this->morosidad = morosidad;
}