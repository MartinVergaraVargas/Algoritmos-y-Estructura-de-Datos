#include <iostream>
using namespace std;

#ifndef ARREGLO_H
#define ARREGLO_H

class Arreglo {
    private:
        int* numeros;
        int posicion;
        int tamano;
        

    public:
        /* constructores */
        Arreglo ();
        Arreglo (int tamano);
        
        /* métodos get */
        int get_suma();
        
        /* métodos set */
        void agregar_numero(int valor);
        
};
#endif
