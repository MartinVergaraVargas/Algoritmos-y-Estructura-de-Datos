#include <iostream>
#include "Arreglo.h"
using namespace std;

int main(){
    int tamano;

    cout << "Ingrese un tamaño para su arreglo de numeros \n";
    cin >> tamano; /* Teniendo el tamaño del arreglo se puede definir la clase Arreglo... */
    Arreglo arreglo = Arreglo(tamano);

    /* ... Para luego simplemente agregarle los datos en un ciclo con tantas repeticiones como lo indique el "tamaño" */
    for(int i=0; i < tamano; i++){ 
        cout << "Ingrese un valor: \n";
        int valor;
        cin >> valor;
        arreglo.agregar_numero(valor); /* Se agregan los valores al Arreglo */
    }

    cout << arreglo.get_suma() << " Es el resultado de la suma de los cuadrados de su arreglo. \n";

}
