#include <iostream>
#include "Arreglo.h"
using namespace std;

///////////////////////////////////////////////////////////////////////
/* constructores */
Arreglo::Arreglo() {
    int* numeros;
    int posicion;
    int tamano;
}
Arreglo::Arreglo (int tamano) {
    this->tamano;
    this->numeros = new int[this->tamano];
    this->posicion = 0; /* variable necesaria para definir los indices de los elementos ingresados al Arreglo. */
}
///////////////////////////////////////////////////////////////////////
/* métodos get */
int Arreglo::get_suma() {
    int sumatoria = 0;
    for(int i=0; i < this->tamano; i++){

        /* operatoria en que se define el valor de 'sumatoria' como la sumatoria de todos los cuadrados de los valores en el Arreglo. */
        sumatoria = sumatoria + (this->numeros[i]*this->numeros[i]);
    }
    return sumatoria;
}

/*int* Arreglo::get_arreglo(){
    
}*/


///////////////////////////////////////////////////////////////////////
/* métodos set */
void Arreglo::agregar_numero(int valor) {

    /* Esta es la funcion que se encarga de ingresar cada valor entregado por el usuario al Arreglo en la posicion 'posicion'. */

    this->numeros[this->posicion] = valor;
    this->posicion = this->posicion +1;

}
      

