#include <iostream>
#include "Frase.h"
using namespace std;

#ifndef ARREGLO_H
#define ARREGLO_H

class Arreglo {
    private:
        Frase* oraciones;
        int posicion;
        int tamano;
        

    public:
        /* constructores */
        Arreglo ();
        Arreglo (int tamano);
        
        /* métodos get */
        /*int get_suma();*/
        
        /* métodos set */
        void agregar_frase(char* fracesitas);
        
};
#endif
