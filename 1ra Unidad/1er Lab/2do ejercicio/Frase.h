#include <iostream>
using namespace std;

#ifndef FRASE_H
#define FRASE_H

class Frase {
    private:
        char* caracteres;
        int mayusculas;
        int minusculas;
        /*int tamano;*/
        

    public:
        /* constructores */
        Frase ();
        Frase (char* caracteres, int tamano);
        
        /* métodos get */
        char* get_caracteres();
        int get_mayusculas();
        int get_minusculas();
        int get_largo();
        
        /* métodos set */
        
};
#endif
