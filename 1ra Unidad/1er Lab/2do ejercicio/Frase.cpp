#include <iostream>
#include <cctype>
#include <string.h>
#include "Frase.h"
using namespace std;

/* constructores */
Frase::Frase() {
    char* caracteres;
    int mayusculas = 0;
    int minusculas = 0;
}
Frase::Frase (char* caracteres, int tamano) {
    /*this->tamano;*/
    this->caracteres = new char[tamano];
    this->caracteres = caracteres;
}
///////////////////////////////////////////////////////////////////////
/* métodos get */
char* Frase::get_caracteres() {    
    return this->caracteres;
}

int Frase::get_mayusculas(){
    for (int i=0; i<strlen(this->caracteres); i++)
    {
        if (isupper(this->caracteres[i])){
            this->mayusculas++;
        }
            
    }
    return this->mayusculas;
}

int Frase::get_minusculas(){
    for (int i=0; i<strlen(this->caracteres); i++)
    {
        if (islower(this->caracteres[i])){
            this->minusculas++;
        }           
    }
    return this->minusculas;
}

int Frase::get_largo(){
    return strlen(this->caracteres);
}