#include <iostream>
#include "Frase.h"
#include <string.h>
using namespace std;

int main(){
    char caracteres[500]; 
    cout << "Ingrese una frase \n";
    cin >> caracteres;
    int tamano = strlen(caracteres);  
                                     
    Frase frase = Frase(caracteres, tamano); 
    cout << "Frase: " << frase.get_caracteres() << "\n";
    cout << "Largo: " << frase.get_largo() << "\n";
    cout << "Mayusculas: " << frase.get_mayusculas() << "\n";
    cout << "Minusculas: " << frase.get_minusculas() << "\n";
}
/*
#include <iostream>
#include <cctype>
#include "Frase.h"
#include "Arreglo.h"
using namespace std;

int main(){
    char*  caracteres;
    
    /*int tamano;

    cout << "Ingrese el tamano de su arreglo de frases: \n";
    cin >> tamano;
    Arreglo texto = Arreglo(tamano);



    for(int i=0; i < tamano; i++){
        cout << "Ingrese una frase \n";
        cin >> caracteres;
        texto.agregar_frase(caracteres);
    }

    cout << "Ingrese una frase \n";
    cin >> caracteres;
    Frase frase = Frase(caracteres);
    
    cout << frase.get_caracteres();
    


    


    /*cout << frase.get_caracteres() << "\n";

}
*/