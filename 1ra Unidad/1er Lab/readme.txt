Laboratorio 1:

En este laboratorio ponemos a prueba conocimientos sobre clases, el manejo de sus atributos y el manejo de ellas 
en arreglos y jerarquías dependiendo del problema a resolver.

En el ejercicio 1, bastante intuitivo, se crea una clase Arreglo que se llena de valores numéricos ingresados por 
teclado, que luego serán leídos por el programa para calcular la sumatoria de sus cuadrados. No hay mucho que decir
al respecto, ya que las instrucciones las va dando el mismo programa.

El ejercicio 2 no está completo, puesto que hubo un fallo en mi entendimiento de las instrucciones. Hice 2 clases
que deben trabajar en conjunto (como una matriz), pero el programa sólo implementa una de las 2 clases hasta el momento.

El ejercicio 3 es el que me pareció el más lógico de hacer y el más entretenido. El programa pide al usuario una 
serie de datos que serán los atributos de la clase Cliente que serán creados tantas veces como quiera el usuario
para llenar un arreglo de Clientes. Luego el usuario puede pedir los datos de los clientes tantas veces como quiera.
La idea era poder actualizar los datos después, pero no me dio el tiempo.
