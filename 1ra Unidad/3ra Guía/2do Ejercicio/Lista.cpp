#include <iostream>
using namespace std;
#include "Lista.h"
#include "Nodo.h"
Lista::Lista() {}

void Lista::crear (int numero) {
    Nodo *tmp;    
    tmp = new Nodo;/* se crea un nodo */    
    tmp->numero = numero;
    tmp->sig = NULL;/* por defecto se iguala a null */

    if (this->raiz == NULL) { /* el primer nodo se asigna como raiz y como ultimo nodo */
        this->raiz = tmp;
        this->ultimo = this->raiz;
  
    } else { /*En caso contrario, el ultimo noso apunta al nuevo ingreso y lo deja como último de la lista */
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}

Nodo* Lista::getRaiz(){
    return this->raiz;
}
void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout << tmp->numero << " ";
        tmp = tmp->sig;
    }
    cout << endl;
}
void Lista::fusionar(Lista* lista){
    this->ultimo->sig = lista->getRaiz();
}