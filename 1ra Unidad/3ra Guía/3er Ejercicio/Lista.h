#include <iostream>
using namespace std;
#include "Nodo.h"
#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Persona. */
        void crear (int numero);
        /* imprime la lista. */
        Nodo* getRaiz();
        Nodo* getUltimo();
        void imprimir();
        void rellenar();
};
#endif
