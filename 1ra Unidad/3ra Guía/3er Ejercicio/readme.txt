Una vez más el programa inicia permitiendo al usuario llenar la lista hasta que se le antoje ingresar
la "f", en este caso el ingreso se terminará y se imprimirá una lista que completa los números faltantes
entre los que fueron ingresados por el usuario. El programa no permite ingresar números menores que 
los ingresados anteriormente, pero por descuido mío sí permite que se repitan los números.