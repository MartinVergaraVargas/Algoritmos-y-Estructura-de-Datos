#include <iostream>
using namespace std;
#include "Nodo.h"
#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        
        void crear (int numero);
        /* imprime la lista. */
        void imprimir();
};
#endif
