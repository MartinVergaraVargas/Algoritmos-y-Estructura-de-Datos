En este primer ejercicio se leen numeros ingresados por terminal y son agregados uno a uno
a la lista del programa. Mi comprensión lectora debe haber estado de vacaciones, porque acabo
de notar que los pide ordenados. En el programa los números se ingresan uno a uno hasta que el
mismo usuario le ponga fin al ingreso usando la tecla "f".