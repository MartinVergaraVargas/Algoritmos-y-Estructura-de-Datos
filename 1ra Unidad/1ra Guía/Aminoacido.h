#include <iostream>
#include "Atomo.h"
#include <list>
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
    private:
        string nombre;
        int numero;
        list<Atomo> atomos;
    public:
        /* constructores */
        Aminoacido ();
        Aminoacido (string nombre, int numero, list<Atomo> atomos);
        
        /* métodos get */
        string get_nombre();
        int get_numero();
        list<Atomo> get_atomos();

        /* métodos set */
        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_atomos(list<Atomo> atomos);
};
#endif
