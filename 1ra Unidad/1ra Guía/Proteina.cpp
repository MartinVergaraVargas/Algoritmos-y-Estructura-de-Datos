#include <iostream>
#include "Cadena.h"
#include "Proteina.h"
#include <list>
using namespace std;
///////////////////////////////////////////////////////////////////////
/* constructores */
Proteina::Proteina() {
    string nombre;
    string id;
    list<Cadena> cadenas;
}
Proteina::Proteina (string nombre, string id, list<Cadena> cadenas) {
    this->nombre = nombre;
    this->id = id;
    this->cadenas = cadenas;
}
///////////////////////////////////////////////////////////////////////
/* métodos get */
string Proteina::get_nombre() {
    return this->nombre;
}
string Proteina::get_id() {
    return this->id;
}
list<Cadena> Proteina::get_cadenas(){
  return this->cadenas;
}
///////////////////////////////////////////////////////////////////////
/* métodos set */
void Proteina::set_nombre(string nombre) {
    this->nombre = nombre;
}
void Proteina::set_id(string id) {
    this->id = id;
} 
void Proteina::set_cadenas(list<Cadena> cadenas) {
    this->cadenas = cadenas;
}
        

