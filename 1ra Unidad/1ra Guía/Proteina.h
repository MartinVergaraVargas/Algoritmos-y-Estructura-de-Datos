#include <iostream>
#include "Cadena.h"
#include <list>
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
    private:
        string nombre;
        string id;
        list<Cadena> cadenas;

    public:
        /* constructores */
        Proteina ();
        Proteina (string nombre, string id, list<Cadena> cadenas);
        
        /* métodos get */
        string get_nombre();
        string get_id();
        list<Cadena> get_cadenas();
        
        /* métodos set */
        void set_nombre(string nombre);
        void set_id(string id);
        void set_cadenas(list<Cadena> cadenas);
};
#endif
