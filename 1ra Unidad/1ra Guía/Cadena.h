#include <iostream>
#include "Aminoacido.h"
#include <list>
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
    private:
        string letra;
        list<Aminoacido> aminoacidos;

    public:
        /* constructores */
        Cadena ();
        Cadena (string letra, list<Aminoacido> aminoacidos);
        
        /* métodos get */
        string get_letra();
        list<Aminoacido> get_aminoacidos();
        
        /* métodos set */
        void set_letra(string letra);
        void set_aminoacidos(list<Aminoacido> aminoacidos);
};
#endif
