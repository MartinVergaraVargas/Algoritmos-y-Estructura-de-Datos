#include <iostream>
#include "Aminoacido.h"
#include "Cadena.h"
#include <list>
using namespace std;
///////////////////////////////////////////////////////////////////////
/* constructores */
Cadena::Cadena() {
    string letra;
    list<Aminoacido> aminoacidos;
}
Cadena::Cadena (string letra, list<Aminoacido> aminoacidos) {
    this->letra = letra;
    this->aminoacidos = aminoacidos;
}
///////////////////////////////////////////////////////////////////////
/* métodos get */
string Cadena::get_letra() {
    return this->letra;
}
list<Aminoacido> Cadena::get_aminoacidos(){
  return this->aminoacidos;
}
///////////////////////////////////////////////////////////////////////
/* métodos set */
void Cadena::set_letra(string letra) {
    this->letra = letra;
}        
void Cadena::set_aminoacidos(list<Aminoacido> aminoacidos) {
    this->aminoacidos = aminoacidos;
}
        

