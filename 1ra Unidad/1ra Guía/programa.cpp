#include <iostream>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
#include <list>
using namespace std;

int main(){
    
    /* VARIABLES */
    string nombre_proteina, id_proteina, letra_cadena = " ", nombre_aminoacido = " ", nombre_atomo = " ";
    float x, y, z;
    
    /* definicion de objetos (El problema con este programa es que los objetos se crean una sola vez y no en cada vuelta de cada ciclo
     * eso sera arreglado en la siguiente version del programa) */
     
    Proteina proteina = Proteina();
    Cadena cadena = Cadena();
    Aminoacido aminoacido = Aminoacido();
    Atomo atomo = Atomo();
    Coordenada coordenada = Coordenada();
    list<Proteina> proteinas;
    
    while(true){        /* ciclo para crear proteinas */
        cout << "Ingrese el nombre_proteina de la proteina: (Para finalizar escriba 'salir'): ";
        cin >> nombre_proteina;
        if(nombre_proteina == "salir"){
            break;
        }
        proteina.set_nombre(nombre_proteina);
        cout << "(" << nombre_proteina << ")Ingrese el ID de la proteina: ";
        cin >> id_proteina;
        proteina.set_id(id_proteina);
        
        while(true){        /* ciclo para crear cadenas */
            cout << "(" << nombre_proteina << ")Ingrese letra de la cadena (Para finalizar escriba 'salir'): ";
            cin >> letra_cadena;
            if(letra_cadena == "salir"){
                break;
            }
            cadena.set_letra(letra_cadena);
            int numero_aminoacido = 1;
            list<Aminoacido> aminoacidos;
            
            while(true){        /* ciclo para crear aminoacidos */
                cout << "(" << nombre_proteina << " " << letra_cadena << ")Ingrese nombre del aminoácido (Para finalizar escriba 'salir'): ";
                cin >> nombre_aminoacido;
                if(nombre_aminoacido == "salir"){
                    break;
                }
                aminoacido.set_nombre(nombre_aminoacido);
                aminoacido.set_numero(numero_aminoacido);
                int numero_atomo = 1;
                
                while(true){        /* ciclo para crear atomos */
                    cout << "(" << nombre_proteina << " " << letra_cadena << " " << nombre_aminoacido << ")Ingrese nombre del atomo (Para finalizar escriba 'salir'): ";
                    cin >> nombre_atomo;
                    if(nombre_atomo == "salir"){
                        break;
                    }
                    atomo.set_nombre(nombre_atomo);
                    atomo.set_numero(numero_atomo);
                    cout << "Ingrese coordenadas del átomo: \n";
                    cout << "x: ";
                    cin >> x;
                    cout << "y: ";
                    cin >> y;
                    cout << "z: ";
                    cin >> z;
                    coordenada.set_x(x);
                    coordenada.set_y(y);
                    coordenada.set_z(z);
                    atomo.set_coordenada(coordenada);
                    numero_atomo++;
                }
                numero_aminoacido++;
                aminoacidos.push_back(aminoacido);
            }
        }
        proteinas.push_back(proteina);
    }
    
    cout << "Imprimir resultados\n";
    
    for (Proteina proteina: proteinas){
        cout << "Nombre: " << proteina.get_nombre() << "\n";
        cout << "ID: " << proteina.get_nombre() << "\n";
        
        for(Cadena cadena: proteina.get_cadenas()){
            cout << "Letra: " << cadena.get_letra() << "\n";
            
            for(Aminoacido aminoacido: cadena.get_aminoacidos()){
                cout << "Nombre aminoacido" << aminoacido.get_nombre() << "\n";
                cout << "Numero aminoacido" << aminoacido.get_numero() << "\n";
                
                for(Atomo atomo: aminoacido.get_atomos()){
                    cout << "Nombre atomo" << atomo.get_nombre() << "\n";
                    cout << "X: " << atomo.get_coordenada().get_x << "\n"();
                    cout << "Y: " << atomo.get_coordenada().get_y() << "\n";
                    cout << "Z: " << atomo.get_coordenada().get_z() << "\n";
                }
            }
        }
    }
}
