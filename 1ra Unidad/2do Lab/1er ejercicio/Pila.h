#include <iostream>
using namespace std;
#ifndef PILA_H
#define PILA_H
class Pila{
    /*Constructores*/
    private: 
        int* pila;
        int tope;
        bool band;
        int max;
    public: 
        Pila();
        Pila(int max);

        /* Funciones de apilar y desapilar datos (además de las 2 funciones para checkear que la
        Pila no se desborde ni se subdesborde)*/
        void Pila_Vacia();
        void Pila_Llena();
        void Push(int dato);
        void Pop();

        /* Métodos get */
        void get_Pila();
        int get_Tope();
        int get_Max();
        bool get_Band();
};
#endif