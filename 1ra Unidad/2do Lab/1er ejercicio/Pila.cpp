#include <iostream>
#include "Pila.h"
using namespace std;

///////////////////////////////////////////////////////////////////////
/* constructores */
Pila::Pila(){
    int* pila;
    int tope;
    bool band;
    int max;
}
Pila::Pila(int max){
        this->max = max;
        this->pila = new int[this->max];
        this->tope = 0;
        this->band = true;
}
///////////////////////////////////////////////////////////////////////

void Pila::Pila_Vacia(){
    if(this->tope == 0){
        this->band = true;
    }
    else{
        this->band = false;
    }
}
///////////////////////////////////////////////////////////////////////

void Pila::Pila_Llena(){
    if(this->tope == this->max){
        this->band = true;
    }
    else{
        this->band = false;
    }
}
///////////////////////////////////////////////////////////////////////

void Pila::Push(int dato){
    Pila_Llena();
    if(this->band == true){
        cout << "\nDesbordamiento, pila llena\n";
    }
    else{
        this->pila[this->tope] = dato;
        this->tope = this->tope + 1;
    }
}
///////////////////////////////////////////////////////////////////////

void Pila::Pop(){
    Pila_Vacia();
    if(this->band == 1){
        cout << "\nSubdesbordamiento, Pila vacía\n";
    }
    else{
        int dato = this->pila[this->tope];
        this->tope--;
    }
}
///////////////////////////////////////////////////////////////////////

void Pila::get_Pila(){
    cout << "\nEsta es su Pila: \n";
    cout << "\n";

    cout << ".-. \n";
    for(int i = this->tope-1; i>=0; i--){
        cout << "|" << this->pila[i] << "|" << "\n";
    }
    cout << "''' \n";
}
///////////////////////////////////////////////////////////////////////

int Pila::get_Tope(){
    return this->tope;
}
///////////////////////////////////////////////////////////////////////
int Pila::get_Max(){
    return this->max;
}
///////////////////////////////////////////////////////////////////////
bool Pila::get_Band(){
    return this->band;
}
///////////////////////////////////////////////////////////////////////