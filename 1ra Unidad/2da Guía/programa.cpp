#include <iostream>
#include "Pila.h"
#include "Contenedor.h"
using namespace std;
int main(int argc, char *argv[]){ /*Forma en que se ingresan los parámetros por terminal al programa*/

	int cantidad_de_pilas = atoi(argv[1]);
	int contenedores_apilados = atoi(argv[2]);
    /*Los las variables que se definen con los parámetros tienen nombres descriptivos para que se 
    entienda mejor el código.*/
    

    Pila* puerto;
    Pila pila = Pila(contenedores_apilados);
    char res;
    string nombre;
    int numero;
    int cant_pilas, tamano_pilas;



    cout << "La cantidad de pilas es: " << cantidad_de_pilas << " pilas\n";
    /*Se le notifica al usuario cuál de los parámetros ingresados corresponde a cada variable 
    del programa.*/
    puerto = new Pila[cantidad_de_pilas];
    cout << "El tamaño de las pilas es: " << contenedores_apilados << " contenedores\n";

    for(int i = 0; i<cantidad_de_pilas; i++){
        puerto[i] = Pila(contenedores_apilados);
    }

    while(res != '0'){ 
        int fila;       /*Mismo menu que el lab 2, pero con la opcion extra de Reubicar*/
        cout << "----------------\n";
        cout << "Agregar/push[1]\nRemover/pop[2]\nReubicar[3]\nVer Pilas[4]\nSalir[0]\n";
        cout << "----------------\n";
        cin >> res; /*Variable respuesta para decidir el estado en el While
                        si la respuesta llega a ser 0 el programa se termina.*/

        if(res == '1'){
            cout << "Seleccionar pila: ";
            cin >> fila;
            cout << "Ingrese empresa: "; 
            
            /*Cuando inicié el programa puse como "nombre" la variable
            que corresponde a la "empresa", sólo lo cambié en las clases, no en el programa. 
            No tiene influencia en el funcionamiento del programa.*/
            cin >> nombre;
            cout << "Ingrese numero: ";
            cin >> numero;
            Contenedor contenedor = Contenedor(nombre, numero);
            puerto[fila].Push(contenedor);
        }

        if(res == '2'){
            cout << "Seleccionar pila: ";
            cin >> fila;
            puerto[fila].Pop();
        }

        if(res == '3'){
            Contenedor cont;
            do{
                cout << "Seleccionar pila a extraer: ";
                cin >> fila; 
            }while(puerto[fila].getBand() == 1);
            cont = puerto[fila].Pop();
            do{
                cout << "Seleccionar pila a depositar: ";
                cin >> fila; 
            }while(puerto[fila].getBand() == 0);
            puerto[fila].Push(cont);
        }
        
        if(res == '4'){
            for(int i=0; i<cant_pilas; i++){
                cout << "Pila " << i << ": ";
                puerto[i].getPila();
            }
        }
    }
}
