#include <iostream>
#include "Contenedor.h"
using namespace std;
#ifndef PILA_H
#define PILA_H
class Pila{
    private: 
        Contenedor* pila;
        int tope;
        bool band;
        int max;
    public: 
        Pila();
        Pila(int max);
        void PilaVacia();
        void PilaLlena();
        void Push(Contenedor dato);
        Contenedor Pop();
        void getPila();
        int getTope();
        int getMax();
        bool getBand();
};
#endif
