#include <iostream>
using namespace std;
#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor{
    private: 
        string empresa;
        int numero;
    public: 
        Contenedor(){}
        Contenedor(string empresa, int numero);
        string getempresa();
        int getNumero();
};
#endif
