Guía 2:

En esta guía de manejo de arreglos en forma de Pilas se debe iniciar el programa pasando como parámetros
la cantidad de pilas que caben en el puerto y la cantidad de contenedores que se pueden apilar en ellas.

Los contenedores deben tener una empresa y un numero de identificacion, esto no es automático, es decir,
en el programa, una de las opciones secundarias (las que vienen después de seleccionar la acción de "Push",
"Pop" o "Reubicar" un contenedor) es "seleccionar contenedor", esta opcion le permite al usuario nombrar
el contenedor seleccionado, removerlo, reubicarlo o ver su información dependiendo de la selección primaria
hecha.

Falta mucho que pulir de esta versión del programa, pero es lo que alcancé a hacer en esta semana.