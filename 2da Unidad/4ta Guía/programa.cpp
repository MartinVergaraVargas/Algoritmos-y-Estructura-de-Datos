#include <fstream>
#include <iostream>
using namespace std;
typedef struct _Nodo {
    int info;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;
//crea un nuevo nodo.
Nodo *crearNodo(int dato) {
    Nodo *q;
    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->info = dato;
    return q;
}

class Grafo {
    private:
    public:
        Grafo (Nodo *nodo) {
            ofstream fp;
            /* abre archivo */
            fp.open ("grafo.txt");
            fp << "digraph G {" << endl;
            fp << "node [style=filled fillcolor=blue];" << endl;
            crear_archivo(nodo, fp);
            fp << "}" << endl;
            /* cierra archivo */
            fp.close();
            /* genera el grafo */
            system("dot -Tpng -ografo.png grafo.txt &");
            /* visualiza el grafo */
            system("eog grafo.png &");
        }
        //recorre en árbol en preorden y agrega datos al archivo.
        void crear_archivo(Nodo *p, ofstream &fp) {
            string cadena = "\0";
            if (p != NULL) {
                //Izquierda
                if (p->izq != NULL && p->izq->info != NULL) {
                    fp <<  p->info << "->" << p->izq->info << ";" << endl;
                }
                //Derecha
                if (p->der != NULL && p->der->info != NULL) { 
                    fp << p->info << "->" << p->der->info << ";" << endl;
                }
                crear_archivo(p->izq, fp);
                crear_archivo(p->der, fp); 
            }
        }
        
};
void inorden(Nodo *p){ /*Funcion que imprime los datos en recorrido inorden*/
    if(p != NULL){

        if(p->izq != NULL && p->izq->info != NULL){
            inorden(p->izq);   
        }
        cout << p->info << ", "; /*Ubicacion del cout en la funcion*/
        if(p->der != NULL && p->der->info != NULL){
            inorden(p->der);
        }
    }
}
void preorden(Nodo *p){ /*Funcion que imprime los datos en recorrido preornoden*/
    if(p != NULL){

        cout << p->info << ", "; /*Ubicacion del cout en la funcion*/
        if(p->izq != NULL && p->izq->info != NULL){
            preorden(p->izq);   
        }
        if(p->der != NULL && p->der->info != NULL){
            preorden(p->der);
        }
    }
}
void posorden(Nodo *p){ /*Funcion que imprime los datos en recorrido posonoden*/
    if(p != NULL){

        if(p->izq != NULL && p->izq->info != NULL){
            posorden(p->izq);   
        }
        if(p->der != NULL && p->der->info != NULL){
            posorden(p->der);
        }
        cout << p->info << ", "; /*Ubicacion del cout en la funcion*/
    }
}

bool verificar_int(string cadena){ /*Función que se encarga de reconocer si el valor ingresado es entero o no*/
    bool respuesta = true;
    for(int i = 0; i<cadena.length(); i++){
        if(!isdigit(cadena[i])){
            respuesta = false;
            break;
        }
    }
    return respuesta;
}

void ingresar_valor(Nodo *p, int valor){ /*Función que se encarga de ingresar los valores al árbol,

se encarga de verificar que el valor ingresado por teclado no haya sido ingresado ya al árbol y de 
crear el árbol en primer lugar. */

    if(valor > p->info){
        if(p->der == NULL){
            Nodo* nuevo = crearNodo(valor);
            p->der = nuevo;
        }
        else{
            ingresar_valor(p->der, valor);
        }
    }
    if(valor < p->info){
        if(p->izq == NULL){
            Nodo* nuevo = crearNodo(valor);
            p->izq = nuevo;
        }
        else{
            ingresar_valor(p->izq, valor);
        }
    }
    if(valor == p->info){
        cout << "Valor ya ingresado" << endl;
    }
}
void buscar(Nodo* p, int valor){ /*Funcion que busca un valor numerico en el arbol*/

    if(p->info > valor){
        if(p->izq == NULL){ 
            cout << "El valor no se encuentra en el arbol"<< endl;
        }
        else{
            buscar(p->izq, valor);
        }
    }
    else{
        if(p->info < valor){
            if(p->der == NULL){
                cout << "El valor no se encuentra en el arbol"<< endl;
            }
            else{
                buscar(p->der, valor);
            }
        }
        else{
            cout << "Encontrado: " << p->info << endl;
        }
    }
}
void eliminar(Nodo* p, int valor){ /*Eliminador de valores*/
    bool bo;
    Nodo* otro;
    Nodo* aux;
    Nodo* aux1;
    if(p != NULL){ /*En caso de que el árbol esté vacío, no se ingresa al resto de la función y se le notifica 
    al usuario que no hay nada para eliminar.*/

                   /*En caso de que el árbol no esté vacío, el dato es buscado por el árbol hasta que es 
                   encontrado para su eliminación*/
        if(valor < p->info){ 
            eliminar(p->izq, valor);
        }
        else{
            if(valor > p->info){
                eliminar(p->der, valor);
            }
            else{
                otro = p;
                if(otro->der == NULL){
                    p = otro->izq;
                }
                else{
                    if(otro->izq == NULL){
                        p = otro->der;
                    }
                    else{
                        aux = p->izq;
                        bo = false;
                        while(aux->der != NULL){
                            aux1 = aux;
                            aux = aux->der;
                            bo = true;
                        }
                        p->info = aux->info;
                        otro = aux;
                        if(bo){
                            aux1->der = aux->izq;
                        }
                        else{
                            p->izq = aux->izq;
                        }
                    }
                }
                delete(otro);
            }
        }
    }
    else{
        cout << "Valor no encontrado" << endl;
    }
}
int main(void) { /*Llegamos al programa, aquí se tiene el menu y se hace uso de todas las funciones
                previamente*/
    Nodo *raiz = NULL;
    int response;
        while(1){
        cout << "[1] Ingresar valores\n[2] Buscar un valor\n[3] Eliminar un valor\n[4] Imprimir valores\n[5] Generar grafo\n[0] Salir\n";
        cin >> response;
        while(response > 5 || response < 0){ /*Se verifica que la respuesta ingresada corresponda a una de las opciones*/
            cout << "Opción no válida, intente nuevamente\n";
            cin >> response;
        }
        if(response == 0){ /*Si la respuesta es 0 el programa se cierra*/
            cout << "Bye\n";
            break;
        }
        if(response == 1){ /*Si la respuesta es 1 se ingresan valores hasta que el usuario diga "ya basta!"
        apretando la "f"*/

            if(raiz == NULL){
                int numero; /*En caso de que el arbol no tenga datos ingresados se inicializa aquí.*/
                cout << "Ingrese un valor inicial " << endl;
                cin >> numero;
                raiz = crearNodo(numero);
            }
            string res;
            while(1){
                cout << "Ingrese valor (ingrese f para finalizar):" << endl; /*Aqui se finaliza el ingreso
                de datos al arbol, como se explicó antes.*/
                cin >> res;
                if(res == "f"){
                    break;
                }
                if(!verificar_int(res)){ 

                /*Implementación de la función "verificar_int", este fragmento se encarga de que solo sean
                ingresados valores enteros al árbol. */

                    cout << "Ingrese un valor numérico!! \n" << endl;
                }
                else{
                    int valor = stoi(res);
                    ingresar_valor(raiz, valor);
                }
            }
        }
        if(response == 2){
            if(raiz == NULL){ /*Si el arbol no tiene valores, no se puede buscar nada.*/
                cout << "El árbol se encuentra vacío" << endl;
            }
            else{ /*En caso contrario usamos la funcion vista anteriormente ("buscar")*/
                cout << "Ingrese el valor a buscar" << endl;
                int res;
                cin >> res;
                buscar(raiz, res); 
            }
        }
        if(response == 3){
            if(raiz == NULL){ /*No se pueden eliminar datos si no existen en el arbol.*/
                cout << "El árbol se encuentra vacío" << endl;
            }
            else{ /*Pero si en caso de que si existan en el, en este caso se usa la funcion "eliminar".*/
                cout << "Ingrese el valor a eliminar" << endl;
                int res;
                cin >> res;
                eliminar(raiz, res);
            }

        }
        if(response == 4){ /*En este fragmento se imprimen los datos del arbol en el orden que seleccione el usuario.*/
            if(raiz == NULL){ /*Siempre y cuando el arbol tenga datos en el.*/
                cout << "El árbol se encuentra vacío" << endl;
            }
            else{
                cout << "[1] Preorden\n[2] Inorden\n[3] Posorden\n";
                cin >> response;
                while(response > 3 || response < 1){ /*Nuevamente se verifica que no se ingresen respuestas distintas de las opciones*/
                    cout << "Opción no válida, intente nuevamente\n";
                    cin >> response;
                }
                if(response == 1){ /*"preorden" seleccionado, se usa la funcion "preorden" que se vio anteriormente*/
                    cout << "Preorden: " << endl;
                    preorden(raiz);
                    cout << endl;
                }
                if(response == 2){ /*"inorden" seleccionado, se usa la funcion "inorden" que se vio anteriormente*/
                    cout << "Inorden: " << endl;
                    inorden(raiz);
                    cout << endl;
                }
                if(response == 3){ /*"posorden" seleccionado, se usa la funcion "posorden" que se vio anteriormente*/
                    cout << "Posorden: "<< endl;
                    posorden(raiz);
                    cout << endl;
                }
                cout << "\n";
            }
        }
        if(response == 5){
            if(raiz == NULL){ /*Si el arbol esta vacio no se puede graficar nada.*/
                cout << "El árbol se encuentra vacío" << endl;
            }
            else{ /*En caso contrario se grafica lo que haya.*/
                Grafo *g = new Grafo(raiz);  
            }
        }
    }
    return 0;
}