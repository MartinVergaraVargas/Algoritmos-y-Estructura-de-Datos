#include <iostream>
#include <fstream>
#include "Grafo.h"
using namespace std;

//Constructor----Código adaptado desde el código del profe.
Grafo::Grafo(Nodo *raiz) {
  ofstream fp;
  fp.open("grafo.txt");
  fp << "digraph G {\n";
  fp << "node [style=filled fillcolor=yellow];\n";
  fp << "nullraiz [shape=point];\n";
  fp << "nullraiz->\"" << raiz->info << "\" [label=" << raiz->FE <<"];\n";
  PreOrden(raiz, fp);
  fp << "}" << endl;
  fp.close();
  system("dot -Tpng -ografo.png grafo.txt");
  system("eog grafo.png &");
}


void Grafo::PreOrden(Nodo *a, ofstream &fp) {
  string cadena = "\0";
  if (a != NULL) {
    if (a->izq != NULL) {
      fp << "\"" + a->info + "\"->\"" + a->izq->info + "\" [label=" + to_string(a->izq->FE) + "];\n";
    } else{
      fp << "\"" <<  a->info + "i\" [shape=point];\n";
      fp << "\"" << a->info << "\"->\"" << a->info << "i\";\n";
    }
    if (a->der != NULL) {
      fp << "\"" + a->info + "\"->\"" + a->der->info + "\" [label=" + to_string(a->der->FE) + "];\n";
    } else{
      fp << "\"" <<  a->info + "d\" [shape=point];\n";
      fp << "\"" << a->info << "\"->\"" << a->info << "d\";\n";
    }
    PreOrden(a->izq,fp);
    PreOrden(a->der,fp); 
  }
}