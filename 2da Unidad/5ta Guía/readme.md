# Guía 5

 En esta guía se pide adaptar un código escrito en C a C++.
 El programa consiste en un código capaz de leer archivos para ordenar los datos leídos de ellos. Los datos  consisten en 170000 IDs de proteínas. 
 El orden generado por el código consiste en crear un árbol balanceado a partir de los IDs entregados. Éste árbol balanceado consiste en un árbol binario con un mecanismo balanceado de eliminación e inserción de datos. Esto significa que los nodos en el código cuentan un Factor de Equilibrio a la hora de ordenar los datos entregados, checkeando la rama izquierda y la rama derecha de un nodo, calculando la diferencia entre ambas ramas y asignando un valor, "-1, 0 o 1", dependiendo de si las ramas tienen el mismo nivel, un nivel mayor o menor o si derechamente el nodo no presenta hijes.
 El programa permite al usuario organizar y graficar la cantidad de IDs que este le pida.

 Al Insertar un ID a la estructura de árbol, el código actúa, principalmente, de 2 formas: una es que el dato leído sea menor que el nodo raíz y la otra es que sea mayor que éste. En el primer caso, el string leído se agrega a la izquierda del nodo anterior, esto funciona en base a la recursividad del código, comparando constantemente los datos, es decir, el dato ingresado es analizado a la par del nodo por referencia izquierdo al nodo anterior, si se detecta que éste no existe, el código procede a ubicar el dato ingresado en ese nodo, creando la estructura y dándole un valor diferente de nulo. 
 A la hora de eliminar un dato, el código actúa usando la función EliminacionBalanceado(Nodo **nodocabeza, int *BO, string infor).
 

## Requisitos:
* Sistema operativo Ubuntu versión 18.04 o superior.
* C++

## Para ejecutar el código:
Para compilar el programa es necesario usar el siguiente comando:
* make

Luego, para ejecutar el programa se debe utilizar:
* ./main

## Construido con:
* Sistema operativo: Ubuntu 18.04.2.
* Lenguaje de programación: C++.
* Editor de texto: Visual Studio Code.
* Compilador de C++: G++.
* Programa para diseñar los grafos: Graphviz.
* Visualizador de imágenes de Ubuntu.

## By:
* Martín Vergara Vargas.