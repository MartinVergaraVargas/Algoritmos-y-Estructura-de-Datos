#ifndef NODO_H
#define NODO_H
#include <iostream>
using namespace std;

//declaración de la clase nodo
typedef struct _Nodo {
    string info;
    int FE;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;
#endif