#ifndef GRAFO_H
#define GRAFO_H
#include <iostream>
using namespace std;
#include "Nodo.h"

//Constructor declarado en la clase
class Grafo {
    private: 
    public://función de orden.
        Grafo (Nodo *ArbolInt);
        void PreOrden(Nodo *a, ofstream &fp);
};
#endif