#include <iostream>
#include "Seleccion.h"
using namespace std;
int* Seleccion::Ordenar(int* a, int n){
    int i, menor, k, j;
    for (i=0; i<=n-2; i++) {
        menor = a[i];
        k = i;
        for (j=i+1; j<=n-1; j++) {
            if (a[j] < menor) {
            menor = a[j];
            k = j;
        }
    }
    a[k] = a[i];
    a[i] = menor;
    }
    return a;
}