# Guía 8

 El código genera un vector de N números aleatorios, donde N es ingresado al ejecutar el programa como un parámetro. Luego son aplicados 2 algoritmos de ordenamiento de datos que corresponden a los métodos "selección" y "quicksort" comparando el tiempo que se toma cada uno en ordenar los datos del vector.
 Además de lo anterior, el código cuenta con una opción de visualización del vector, es decir, luego de ejecutar el programa, éste le pregunta al usuario si quiere imprimir (1) la serie de datos ordenada de ambas formas o si no quiere imprimir tal información (0) donde no imprimirla significa que al finalizar su trabajo, el código sólo mostrará cuánto tiempo invirtió cada algoritmo en ordenar el vector, mientras que la opción de imprimirla muestra este tiempo invertido y el resultado del ordenamiento hecho por cada algoritmo.
 
 Luego de hacer la selección "ver o no" el código valida la entrada del parámetro N para que este no sea menor que 0 ni mayor que 1.000.000 y genera la selección de datos aleatorios para llenar el vector. 

## Requisitos:
* Sistema operativo Ubuntu versión 18.04 o superior.
* C++

## Para ejecutar el código:
Para compilar el programa es necesario usar el siguiente comando:
* make

Luego, para ejecutar el programa se debe utilizar:
* ./programa número

Donde número corresponde a la cantidad de datos a ordenar.

Luego el menú le pregunta al usuario si quiere (1) o no (2) ver los datos en su orden final.

## Construido con:
* Sistema operativo: Ubuntu 18.04.2.
* Lenguaje de programación: C++.
* Editor de texto: Visual Studio Code.
* Compilador de C++: G++.

## By:
* Martín Vergara Vargas.