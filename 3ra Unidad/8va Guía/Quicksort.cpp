#include <iostream>
#include "Quicksort.h"
using namespace std;
int* Quicksort::Ordenar(int* a, int n){
    int tope, ini, fin, pos;
    int pilamenor[100];
    int pilamayor[100];
    int izq, der, aux, band;
    tope = 0;
    pilamenor[tope] = 0;
    pilamayor[tope] = n-1;
    while (tope >= 0) {
        ini = pilamenor[tope];
        fin = pilamayor[tope];
        tope = tope - 1;
        // reduce
        izq = ini;
        der = fin;
        pos = ini;
        band = true;
        while (band == true) {
            while ((a[pos] <= a[der]) && (pos != der))
                der = der - 1;
            if (pos == der) {
                band = false;
            } else {
                aux = a[pos];
                a[pos] = a[der];
                a[der] = aux;
                pos = der;
                while ((a[pos] >= a[izq]) && (pos != izq))
                    izq = izq + 1;  
                if (pos == izq) {
                    band = false;
                } else {
                    aux = a[pos];
                    a[pos] = a[izq];
                    a[izq] = aux;
                    pos = izq;
                }
            }
        }
        if (ini < (pos - 1)) {
            tope = tope + 1;
            pilamenor[tope] = ini;
            pilamayor[tope] = pos - 1;
        }
        if (fin > (pos + 1)) {
            tope = tope + 1;
            pilamenor[tope] = pos + 1;
            pilamayor[tope] = fin;
        }
    }
    return a;
}