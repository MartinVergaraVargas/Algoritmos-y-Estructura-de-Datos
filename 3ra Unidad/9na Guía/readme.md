# Guía 9: Métodos de búsqueda - Tablas Hash

 El código genera un arreglo de 20 datos numéricos entregados por el usuario y los distrubuye de la manera que el usuario especifique al momento de ejecutar el programa dependiendo de si los datos colisionan o no. Las distribuciones corresponden a 4: Reasignación Lineal, que en el ingreso ordena los datos de tal manera que si colisionan, el dato nuevo se desplazará hacia la derecha tomando el valor de la primera posición vacía; Reasignación Cuadrática, en caso de colisiones moverá los datos hacia la derecha en pasos cuadráticos, es decir, el cuadrado del índice del arreglo en que se encuentra (en caso de i=1, cuadrado=1; caso i=2, cuadrado=4; caso i=3, cuadrado=9; etc.); Reasignación Doble Hash, en que, en caso de colisión, se toma el valor ingresado y se llama a una función recursivamente hasta que se solucione la colisión; Finalmente en el Encadenamiento cada posición del arreglo referencia una lista enlazada a la cual se agrega el valor ingresado en caso de colisión.

## Requisitos:
* Sistema operativo Ubuntu versión 18.04 o superior.
* C++

## Para ejecutar el código:
Para compilar el programa es necesario usar el siguiente comando:
* make

Luego, para ejecutar el programa se debe utilizar:
* ./programa Método_de_Distribución

Donde el Método_de_Distribución se selecciona con las letras L (para Lineal), C (para Cuadrática), D (para Doble Hash) y E (para Encadenamiento).

Luego el menú permite al usuario ingresar los datos hasta que se llene el arreglo.

## Construido con:
* Sistema operativo: Ubuntu 18.04.2.
* Lenguaje de programación: C++.
* Editor de texto: Visual Studio Code.
* Compilador de C++: G++.

## By:
* Martín Vergara Vargas.